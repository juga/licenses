# Tools to find licenses/copyright in software

A [list of tools](https://wiki.debian.org/CopyrightReviewTools)

- [ScanCode toolkit]( https://scancode-toolkit.readthedocs.io/.)
  cli
  Cons:
  - slow
- [scancode.io](https://scancodeio.readthedocs.io/)
  Has cli and Web UI. Possible setup via docker.
  Cons:
  - Crash with 8GB RAM due OOM
- [FOSSology](https://www.fossology.org/)
  Not tested
- [licensecheck](https://metacpan.org/pod/App::Licensecheck)
  cli
  Pros:
  - fast
  - simple install and use
  - seems to detect quite well licenses
  ```bash
  apt install
  # example run for python project
  licensecheck --check '.py' --recursive --deb-machine . > project.license
  ```